#! /bin/bash


SEP=:
TEMP=temp.$$
FILE=options.csv


[ "$FILE" ] || {
	echo "file does not exist. use the FILE variable"
	return 1
}


[ -r "$FILE" -a -w "$FILE" ] || {
	echo " "
	return 1
}


has_key() {
	grep -i -q "^$1$SEP" "$FILE"
}


erase_key() {
	has_key $1 || return
	grep -i -v "^$1$SEP" "$FILE" > "$TEMP"
	mv "$TEMP" "$FILE"
	echo "the register was succesfufy erased."
}


insert_key() {
	local key=$(echo "$1" | cut -d $SEP -f1)
	if has_key "$key"; then
		echo "the key $key already exist."
		return 1
	else
		echo "$*"  >> "$FILE"
		echo "key $key succesfuly created"
	fi
	return 0
}


get_field() {
	local key=${2:-.*}

	grep -i "^$key$SEP" "$FILE" | cut -d $SEP -f $1
}


fields() {
	head -n 1 "$FILE" | tr $SEP \\n
}


get_key() {
	local data=$(grep -i "^$1$SEP" "$FILE")
	local i=0

	[ "$data" ] || return

	fields | while read field; do
		i=$((i+1))
		echo -n "$field: "
		echo "$data" | cut -d $SEP -f $i
	done
}

